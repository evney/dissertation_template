.PHONY : clean

clean :
	$(RM) *.aux *.log *.blg *.bbl *.out *.lot *.lof *.synctex.gz *.toc *.thm
